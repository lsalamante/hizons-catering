<?php
/* Template Name: Packages */
get_header();
?>
<section class="welcome packagesbg">
  <span>Wedding<br><div>Packages</div></span>
  <h1></h1>
  <p>Choosing what food to serve at your wedding reception is one of the most significant decisions in planning.</p>
  <p>The food creates a lasting impression on your guests even after the program has ended.</p>

</section>

<section class="menu">
  <div class="pagewrapper">
    <h3>Menu</h3>
    <p>Have our extensive food services include a full catering which can have a customized menu based upon the specific needs of the client. Our well-oriented and skilled team of waiters and food attendants will serve and attend to your needs on the day of your wedding.</p>
    <article>
      <ul class="rslides" id="menu">
        <li>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
        </li>
        <li>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
          <div>
            <img src="images/menu1.jpg">
            <aside>
              P600-900
            </aside>
            <h4>Venue Package Set 1</h4>
          </div>
        </li>
      </ul>
    </article>
    <p class="center"><a href="#">View All</a></p>
  </div>
</section>

<section class="service-inclusions">
  <div class="pagewrapper">
    <div class="whitebg">
      <article>
        <h3>Service Inclusions</h3>
        <ul>
          <li>
            <h4>Venue Inclusions</h4>
            <p>Lorem ipsum dolor sit amet
              consectetur adipiscing elit</p>
              <p>Nulla quis metus leo</p>
              <p>Sed mi turpis, congue ut porta
                dictum et Orci varius
                natoque penatibus et magnis dis
                parturient montes, nascetur
                ridiculus mus</p>
                <p>Praesent aliquam
                  sagittis nisi sit amet posuere</p>
                  <p>Morbi at tortor libero</p>
                  <p>Donec venenatis commodo
                    magna a dictum</p>
                  </li>
                  <li>
                    <h4>Food &amp; Beverage</h4>
                    <p>Lorem ipsum dolor sit amet
                      consectetur adipiscing elit</p>
                      <p>Nulla quis metus leo</p>
                      <p>Sed mi turpis, congue ut porta
                        dictum et Orci varius
                        natoque penatibus et magnis dis
                        parturient montes, nascetur
                        ridiculus mus</p>
                        <p>Praesent aliquam
                          sagittis nisi sit amet posuere</p>
                          <p>Morbi at tortor libero</p>
                          <p>Donec venenatis commodo
                            magna a dictum</p>
                          </li>
                          <li>
                            <h4>Gifts for the Bride &amp; Groom</h4>
                            <p>Lorem ipsum dolor sit amet
                              consectetur adipiscing elit</p>
                              <p>Nulla quis metus leo</p>
                              <p>Sed mi turpis, congue ut porta
                                dictum et Orci varius
                                natoque penatibus et magnis dis
                                parturient montes, nascetur
                                ridiculus mus</p>
                                <p>Praesent aliquam
                                  sagittis nisi sit amet posuere</p>
                                  <p>Morbi at tortor libero</p>
                                  <p>Donec venenatis commodo
                                    magna a dictum</p>
                                  </li>
                                </ul>
                              </article>
                            </div>
                          </div>
                        </section>

                        <section class="venues vlist">
                          <div class="pagewrapper">
                            <h2>Venues</h2>
                            <p>Look through our compiled list of accredited venues ideal for your wedding ceremony and reception; <br>each classified based on the type of venue, capacity, location, and cost.</p>

                            <ul class="venuelist">
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>North Forbes Pavilion</h3>
                                  <p>Makati City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>Cities Events Place - Bar</h3>
                                  <p>Quezon City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>Blue Gardens - Chateau</h3>
                                  <p>Quezon City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>North Forbes Pavilion</h3>
                                  <p>Makati City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>North Forbes Pavilion</h3>
                                  <p>Makati City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>Cities Events Place - Bar</h3>
                                  <p>Quezon City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>Blue Gardens - Chateau</h3>
                                  <p>Quezon City</p>
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <img src="images/venue1.jpg" alt="">
                                  <h3>North Forbes Pavilion</h3>
                                  <p>Makati City</p>
                                </a>
                              </li>
                            </ul>
                            <div class="pagewrapper2">
                              <div class="search-venues">
                                <ul>
                                  <li>
                                    <select>
                                      <option>Venue Type</option>
                                    </select>
                                  </li>
                                  <li>
                                    <select>
                                      <option>Location</option>
                                    </select>
                                  </li>
                                  <li>
                                    <select>
                                      <option>Size</option>
                                    </select>
                                  </li>
                                  <li><input type="submit" name="Search" value="" class="searchbtn"></li>
                                </ul>

                              </div>
                            </div>
                            <p class="center"><a href="#" class="link">View all venues</a></p>
                          </div>
                        </section>

                        <section class="connectwus">
                          <div class="pagewrapper2">
                            <div class="whitebg">
                              <article>
                                <h3>Connect with Us</h3>
                                <ul>
                                  <li><input type="text" name="" placeholder="Full name"></li>
                                  <li><input type="date" name="" placeholder="Date of event"></li>
                                  <li>
                                    <select>
                                      <option>Type of Event</option>
                                    </select>
                                  </li>
                                  <li><input type="text" name="" placeholder="Venue(if any)"></li>
                                  <li><input type="number" name="" placeholder="Number of persons"></li>
                                  <li><input type="number" name="" placeholder="Contact Number"></li>
                                  <li><input type="email" name="" placeholder="Email"></li>
                                  <li><img src="images/captcha.jpg"></li>
                                  <li><input type="submit" name="" value="SUBMIT"></li>
                                </ul>
                              </article>
                            </div>
                          </div>
                        </section>
                        <?php get_footer();
