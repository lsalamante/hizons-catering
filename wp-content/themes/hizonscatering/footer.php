<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
?>

<footer>
	<div class="footer">
		<div class="pagewrapper2">
			<div class="sitemap">
				<h4>Sitemap</h4>
				<ul>
					<?php
					$main = array(
						'menu'       => 'secondary-nav',
						'items_wrap' => '%3$s'
					);
					wp_nav_menu($main);
					?>
				</ul>
				<ul>
					<li><a href="#">Services</a></li>
					<li><a href="#">Resources</a></li>
					<li><a href="#">Packages</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</div>

			<div class="contactus">
				<h4>Contact Us</h4>
				<p><a href="tel:+6329250107">+63 (2) 925-0107</a></p>
				<p><a href="tel:+6329250103">+63 (2) 925-0103</a></p>
				<p><a href="mailto:info@hizonscatering.com">info@hizonscatering.com</a></p>
				<p>22 Renowned Lane Sanville <br>Project 6, Quezon City</p>
			</div>

			<div class="help">
				<h4>How Can We Help You?</h4>
				<p>For inquiries, contact our agents so that we can help you plan your dream event</p>
				<p class="center"><a href="#">Contact Us</a></p>
			</div>
		</div>
	</div>
	<aside class="copyright">&copy;HIZON'S CATERING 2015</aside>
</footer>

<!-- Scripts -->

<script src="<?php bloginfo('template_url'); ?>js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>js/responsiveslides.min.js"></script>
<script>
$(function () {
	// Testimonials
	$("#testimonials").responsiveSlides({
		auto: true,
		pager: true,
		nav: true,
		speed: 800,
		maxwidth: 800,
		namespace: "transparent-btns"
	});

});
</script>
<script>
$(window).scroll(function(){
	var sticky = $('.sticky'),
	scroll = $(window).scrollTop();

	if (scroll >= 90) sticky.addClass('fixed');
	else sticky.removeClass('fixed');
});
</script>

<?php wp_footer(); ?>
</body>
</html>
