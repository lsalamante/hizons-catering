<?php
// var_dump(wp_get_nav_menu_items('secondary-nav')); die();
?>
<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/styles.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/media-queries.css">

	<!-- if="" lt="" IE="" 9=""></!-->
	<script src="<?php bloginfo('template_url'); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<!-- css3-mediaqueries.js for IE less than 9 -->
	<!--[if lt IE 9]>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/css3-mediaqueries.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="sticky">
			<div class="pagewrapper">
				<section class="logo">
					<a href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/hizons-logo.png" alt="Hizon's Catering" title="Hizon's Catering"></a>
					<aside class="secondarynav">
						<ul>
							<?php
							$main = array(
								'menu'       => 'secondary-nav',
								'items_wrap' => '%3$s'
							);
							wp_nav_menu($main);
							?>
						</ul>
					</aside>
					<aside class="foodtasting"><a href="#">FREE FOOD TASTING</a></aside>
				</section>
			</div>
			<nav>
				<ul>
					<li><a href="#">Services</a>
						<div>
							<div class="pagewrapper2">
								<ul>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Wedding Services">
											<h3>Wedding</h3>
											<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Debut Services">
											<h3>Debut</h3>
											<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Corporate Services">
											<h3>Corporate</h3>
											<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Private Party Services">
											<h3>Private Party</h3>
											<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Kid's Party Services">
											<h3>Kid's Party</h3>
											<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Events Styling">
											<h3>Events Styling</h3>
											<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li><a href="#">Packages</a>
						<div>
							<div class="pagewrapper2">
								<ul>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Wedding Packages">
											<h3>Wedding</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Debut Packages">
											<h3>Debut</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Corporate Packages">
											<h3>Corporate</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Private Party Packages">
											<h3>Private Party</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Kid's Party Packages">
											<h3>Kid's Party</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Events Styling">
											<h3>Events Styling</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
								</ul>
							</div>
						</div>
					</li>
					<li><a href="#">Venues</a></li>
					<li><a href="#">Resources</a>
						<div>
							<div class="pagewrapper2">
								<ul>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Wedding Resources">

											<h3>Wedding</h3>
										</a>
										<p><a href="#">Wedding Planning 101</a></p>
										<p><a href="#">Silver/Golden Wedding 101</a></p>
										<p><a href="#">Church and Ceremony Venues Finder</a></p>
										<p><a href="#">Wedding Supplier Finder</a></p>
										<p><a href="#">Free Wedding Planning Ebook</a></p>
										<p><a href="#">Wedding Planning Blog</a></p>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Corporate Resources">
											<h3>Corporate</h3>
										</a>
										<p><a href="#">Christmas Party 101</a></p>
										<p><a href="#">Seminars and Meetings 101</a></p>
										<p><a href="#">Awarding and Conventions 101</a></p>
										<p><a href="#">Company Celebrations 101</a></p>
										<p><a href="#">Corporate Event Planning Blog</a></p>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Kid's Party Resources">
											<h3>Kid's Party</h3>
										</a>
										<p><a href="#">Baptism 101</a></p>
										<p><a href="#">1st Birthday 101</a></p>
										<p><a href="#">7th Birthday 101</a></p>
										<p><a href="#">Kid's Party 101</a></p>
										<p><a href="#">Kid's Party Blog</a></p>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Debut Resources">
											<h3>Debut</h3>
										</a>
										<p><a href="#">Debut 101</a></p>
										<p><a href="#">Debut Planning Blog</a></p>
									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail1.jpg" alt="Design Resources">
											<h3>Design</h3>
										</a>
										<p>Interdum et malesuada fames ac ante ipsum primis in faucibus</p>

									</li>
									<li>
										<a href="#">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/thumbnail2.jpg" alt="Blog">
											<h3>Blog</h3>
										</a>
										<p><a href="#">Wedding</a></p>
										<p><a href="#">Kid's Party</a></p>
										<p><a href="#">Corporate</a></p>
										<p><a href="#">Debut</a></p>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li><a href="#">Featured Events</a></li>
					<li><a href="#">Locations</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</nav>
		</div>
	</header>
