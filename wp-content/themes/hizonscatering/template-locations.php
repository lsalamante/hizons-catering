<?php
/* Template Name: Locations */
get_header();
?>
<section class="welcome locationsbg">
  <span>Catering Services</span>
  <h1>in Quezon City</h1>
  <p>Hizon’s Catering provide catering services for your private home <br>or the several venues in Quezon City that we are accredited in</p>

</section>

<section class="location-venues">
  <aside>
    <h2>Want to try our menu? <span><a href="#">Register for our Grand Food Testing</a></span></h2>
  </aside>
  <div class="pagewrapper">
    <div class="whitebg2">
      <article>
        <section>
          <p>Hizon's Catering is accredited in the following venues in Quezon City</p>
          <ul class="venuelist">
            <li>
              <a href="#">
                <img src="images/venue1.jpg" alt="">
                <h3>North Forbes Pavilion</h3>

              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/venue1.jpg" alt="">
                <h3>Cities Events Place - Bar</h3>

              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/venue1.jpg" alt="">
                <h3>Blue Gardens - Chateau</h3>

              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/venue1.jpg" alt="">
                <h3>North Forbes Pavilion</h3>

              </a>
            </li>

          </ul>
        </section>
        <aside>
          <h3>Connect with Us</h3>
          <ul>
            <li><input type="text" name="" placeholder="Full name"></li>
            <li><input type="date" name="" placeholder="Date of event"></li>
            <li>
              <select>
                <option>Type of Event</option>
              </select>
            </li>
            <li><input type="text" name="" placeholder="Venue(if any)"></li>
            <li><input type="number" name="" placeholder="Number of persons"></li>
            <li><input type="number" name="" placeholder="Contact Number"></li>
            <li><input type="email" name="" placeholder="Email"></li>
            <li><img src="images/captcha.jpg"></li>
            <li><input type="submit" name="" value="SUBMIT"></li>
          </ul>
        </aside>
      </article>
    </div>
  </div>
</section>

<?php get_footer();
