<?php
/* Template Name: Home */
get_header();
?>

<section class="welcome welcomebg">
  <span>Welcome</span>
  <h1>Hizon's Catering is the #1 catering company in the Philippines</h1>
  <ul>
    <li><a href="#">Packages</a></li>
    <li><a href="#">Menu</a></li>
  </ul>

</section>

<section class="gft">
  <div class="gft-container">
    <article>
      <div>
        <h2>FindDFGHJKJHGF out why we are the<br> right caterer for your event</h2>
        <p>Attend our grand food tasting to try our menu, see our<br> design and experience our services</p>
        <h3>Date: February 4 &amp; 5, 2017</h3>
        <h3>Time: 10am - 8pm</h3>
        <h3>Save the Date Bridal &amp; Debut Fair (SMX Aura)</h3>
        <h3>SMX Convention Center, SM Aura Premier, McKinley Pkwy, Taguig</h3>
        <p class="margtop"><a href="#">Attend Event</a></p>
      </div>
    </article>
  </div>
</section>

<section class="about-us">
  <div class="pagewrapper">
    <article>
      <img src="<?php bloginfo('template_url'); ?>/assets/images/about-img.jpg" alt="About Hizon's Catering">

      <h3>Who We Are</h3>
      <p>Are you planning for your dream wedding? A themed party for your child’s birthday, or a prominent event for your company? </p>
      <p>Let us know and we would love to attend to your needs. Think of us not just a caterer but also your friend who can help you lessen the worry and hassle. We will assist you during the process of conceptualizing, budgeting, planning and especially on the day itself. </p>
      <p>So together, let us make your dream event possible.</p>
      <p class="margtop"><a href="#">Read More</a></p>

    </article>
    <aside class="clients">
      <div class="border1">
        <ul class="rslides" id="testimonials">
          <li>
            <p>Food is exquisitely made. The cuisines were presentable. The taste from appetizer to dessert were heaven. Thumbs up!</p>
            <h5>Alyssa Nicole Pangilinan Satorre</h5>
            <h5>Client</h5>
          </li>
          <li>
            <p>Food is exquisitely made. The cuisines were presentable. The taste from appetizer to dessert were heaven. Thumbs up!</p>
            <h5>Alyssa Nicole Pangilinan Satorre</h5>
            <h5>Client</h5>
          </li>
          <li>
            <p>Food is exquisitely made. The cuisines were presentable. The taste from appetizer to dessert were heaven. Thumbs up!</p>
            <h5>Alyssa Nicole Pangilinan Satorre</h5>
            <h5>Client</h5>
          </li>
        </ul>
      </div>
      <p class="center"><img src="<?php bloginfo('template_url'); ?>/assets/images/clients-logo.jpg" alt="Clients"></p>
    </aside>
  </div>
</section>

<section class="venues">
  <div class="pagewrapper">
    <h2>Have your event in one of the 375 venues we are accredited in</h2>
    <ul class="venuelist">
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/venue1.jpg" alt="">
          <h3>North Forbes Pavilion</h3>
          <p>Makati City</p>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/venue1.jpg" alt="">
          <h3>Cities Events Place - Bar</h3>
          <p>Quezon City</p>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/venue1.jpg" alt="">
          <h3>Blue Gardens - Chateau</h3>
          <p>Quezon City</p>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/venue1.jpg" alt="">
          <h3>North Forbes Pavilion</h3>
          <p>Makati City</p>
        </a>
      </li>
    </ul>
    <div class="pagewrapper2">
      <div class="search-venues">
        <ul>
          <li>
            <select>
              <option>Venue Type</option>
            </select>
          </li>
          <li>
            <select>
              <option>Location</option>
            </select>
          </li>
          <li>
            <select>
              <option>Size</option>
            </select>
          </li>
          <li><input type="submit" name="Search" value="" class="searchbtn"></li>
        </ul>

      </div>
    </div>
    <p class="center"><a href="#" class="link">View all venues</a></p>
  </div>
</section>

<section class="design-inspiration">
  <div class="pagewrapper">
    <h3>Find inspiration from our designs library</h3>
    <p>Browse our design gallery to get inspiration for your coming events.</p>
    <div class="designs">
      <article>
        <img src="<?php bloginfo('template_url'); ?>/assets/images/design1.jpg">
      </article>
      <aside>
        <ul>
          <li><img src="<?php bloginfo('template_url'); ?>/assets/images/design2.jpg"></li>
          <li><img src="<?php bloginfo('template_url'); ?>/assets/images/design3.jpg"></li>
          <li><img src="<?php bloginfo('template_url'); ?>/assets/images/design4.jpg"></li>
        </ul>
      </aside>
    </div>
    <p class="center"><a href="#">View all designs</a></p>
  </div>
</section>

<section class="tools">
  <div class="pagewrapper">
    <h3>Some tools were created to help you plan your event</h3>
    <ul>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/mobileapp.jpg">
          <h4>Mobile App</h4>
          <p>mobile</p>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/themebook.jpg">
          <h4>Theme Book</h4>
          <p>theme</p>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/ebook.jpg">
          <h4>Ebook</h4>
          <p>ebook</p>
        </a>
      </li>
      <li>
        <a href="#">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/planningvideos.jpg">
          <h4>Planning Videos</h4>
          <p>planning</p>
        </a>
      </li>
    </ul>
  </div>
</section>

<section class="custom-catering-package">
  <div class="pagewrapper">
    <article>
      <h3>We would love to customize for you a catering package that suit your exact needs</h3>
      <p>Share with us a few details about your event. Our event planner will contact you as soon as possible</p>
    </article>
    <aside>
      <ul>
        <li><input type="text" name="" placeholder="Full name"></li>
        <li><input type="date" name="" placeholder="Date of event"></li>
        <li>
          <select>
            <option>Type of Event</option>
          </select>
        </li>
        <li><input type="text" name="" placeholder="Venue(if any)"></li>
        <li><input type="number" name="" placeholder="Number of persons"></li>
        <li><input type="number" name="" placeholder="Contact Number"></li>
        <li><input type="email" name="" placeholder="Email"></li>
        <li><img src="<?php bloginfo('template_url'); ?>/assets/images/captcha.jpg"></li>
        <li><input type="submit" name="" value="SUBMIT"></li>
      </ul>
    </aside>
  </div>
</section>

<section class="featured-events">
  <div class="pagewrapper">
    <h3>Featured Events</h3>
    <ul>
      <li>
        <a href="#">
          <div class="imgsquare">
            <img src="<?php bloginfo('template_url'); ?>/assets/images/event1.jpg">
          </div>
          <h4>Set Forth with Faith: Catholic Educational Association of the Philippines @ 75th</h4>
        </a>
        <p>"Each friend represents a world in us, a world not
          born until they arrive, and...</p>
          <p><a href="#">Read more</a></p>
        </li>
        <li>
          <div class="imgsquare">
            <img src="<?php bloginfo('template_url'); ?>/assets/images/event1.jpg">
          </div>
          <h4>Timeless Friendship,<br>
            Friendship to Romance | Mark &amp; Apple</h4>
            <p>"There are two sorts of romantics: those who love,
              and those who love the adve...</p>
              <p><a href="#">Read more</a></p>
            </li>
            <li>
              <div class="imgsquare">
                <img src="<?php bloginfo('template_url'); ?>/assets/images/event1.jpg">
              </div>
              <h4>The Love Adventure of Doy &amp; Gadi</h4>
              <p>"There are two sorts of romantics: those who love,
                and those who love the adve...</p>
                <p><a href="#">Read more</a></p>
              </li>
            </ul>
            <div class="filter-events">
              <ul>
                <li>Filter Events:</li>
                <li><a href="#">Weddings</a></li>
                <li><a href="#">Debut</a></li>
                <li><a href="#">Corporate</a></li>
                <li><a href="#">Private Party</a></li>
                <li><a href="#">Kid's Party</a></li>
              </ul>
            </div>
          </div>
        </section>

        <section class="blog">
          <div class="pagewrapper">
            <h3>Blog</h3>
            <ul>
              <li>
                <a href="#">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/blog1.jpg">
                  <h4>Top 10 Wedding Designs...</h4>
                </a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                  elit, sed do eiusmod tempor incididunt ut labore et </p>
                  <p><a href="#">Read more</a></p>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/blog1.jpg">
                    <h4>Debuts for the Digital Age.</h4>
                  </a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit, sed do eiusmod tempor incididunt ut labore et </p>
                    <p><a href="#">Read more</a></p>
                  </li>
                  <li>
                    <a href="#">
                      <img src="<?php bloginfo('template_url'); ?>/assets/images/blog1.jpg">
                      <h4>Top 10 Wedding Designs...</h4>
                    </a>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                      elit, sed do eiusmod tempor incididunt ut labore et </p>
                      <p><a href="#">Read more</a></p>
                    </li>
                    <li>
                      <a href="#">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/blog1.jpg">
                        <h4>Debuts for the Digital Age.</h4>
                      </a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt ut labore et </p>
                        <p><a href="#">Read more</a></p>
                      </li>
                    </ul>
                  </div>
                </section>
                <?php get_footer();
